package anton.service;

import anton.bean.Thing;

import java.util.List;

public interface IThingService {

    void save(Thing thing);

    void saveAll(List<Thing> things);
}
