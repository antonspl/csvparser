package anton.service;

import anton.bean.Thing;
import anton.repository.ThingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ThingService implements IThingService {

    @Autowired
    private ThingRepository thingRepository;

    @Override
    public void save(Thing thing) {
        thingRepository.save(thing);
    }

    @Override
    public void saveAll(List<Thing> things){
        for (Thing thing : things) {
            save(thing);
        }
    }
}
