package anton.bean;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "THINGS")
public class Thing {
    @Id
    private Long id;

    private String name;
    private double value;

    public Thing() {
    }

    public Thing(Long id, String name, double value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getValue() {
        return value;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setValue(double value) {
        this.value = value;
    }

    @Override
    public String toString(){
        return "Thing(" + id + ", name=" + name + ", value=" + value + "}";
    }
}
