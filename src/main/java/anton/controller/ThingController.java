package anton.controller;

import anton.bean.Thing;
import anton.service.IThingService;
import au.com.bytecode.opencsv.CSVReader;
import au.com.bytecode.opencsv.bean.ColumnPositionMappingStrategy;
import au.com.bytecode.opencsv.bean.CsvToBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.List;
import java.util.logging.Logger;

@Component
public class ThingController {

    final static Logger logger = Logger.getLogger(ThingController.class.getName());


    @Autowired
    private IThingService thingService;

    @Value("${parent.directory}")
    private String parentDirectory;

    @Value("${handled.directory}")
    private String handledDirectory;

    @Value("${wrong.files..directory}")
    private String wrongFilesDirectoryFile;

    private File directory;
    private File handledDirectoryFile;

    @PostConstruct
    public void init() {
        directory = new File(parentDirectory);
        handledDirectoryFile = new File(handledDirectory);
    }

    @Scheduled(fixedDelay = 1000)
    public void process() {
        if (directory.isDirectory() && directory.list().length == 0) {
            logger.info("No files in the directory");
        } else {
            for (File file : directory.listFiles()) {
                List<Thing> things = parseCsvFile(file.getAbsolutePath());
                if (things.size() != 0) {
                    thingService.saveAll(things);
                    try {
                        File file1 = new File(handledDirectoryFile, file.getName());
                        Files.move(file.toPath(), file1.toPath());
                        logger.info(file.getName() + " handled");
                    } catch (IOException e) {
                        logger.severe("No handled directory");
                    }
                } else try {
                    File file1 = new File(wrongFilesDirectoryFile, file.getName());
                    Files.move(file.toPath(), file1.toPath());
                    logger.severe(file.getName() + " replaced to wrongFilesDirectory");
                } catch (IOException e) {
                    logger.severe("No wrongFilesDirectory directory");
                }
            }
        }
    }

    public List<Thing> parseCsvFile(String csvFilePath) {
        try (CSVReader csvReader = new CSVReader(new FileReader(csvFilePath))) {
            CsvToBean csv = new CsvToBean();
            csvReader.readNext();
            List<Thing> list = (List<Thing>) csv.parse(setColumnMapping(), csvReader);
            return list;
        } catch (IOException e) {
            logger.severe("No files!");
        } catch (RuntimeException r) {
            logger.severe("Wrong file format!");
        }
        return Collections.emptyList();
    }

    private ColumnPositionMappingStrategy setColumnMapping() {
        ColumnPositionMappingStrategy strategy = new ColumnPositionMappingStrategy();
        strategy.setType(Thing.class);
        String[] columns = new String[]{"id", "name", "value"};
        strategy.setColumnMapping(columns);
        return strategy;
    }
}
