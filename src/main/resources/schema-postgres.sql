DROP TABLE IF EXISTS things;
CREATE TABLE things(id serial PRIMARY KEY, name VARCHAR(100), value float);
INSERT into things (id, name, value) values (1, 'vv', 1.22)
